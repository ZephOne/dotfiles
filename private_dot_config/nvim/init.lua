vim.cmd 'let g:nord_disable_background=v:true'
vim.cmd [[packadd packer.nvim]]
require('packer').startup(function()
  -- Packer can manage itself as an optional plugin
  use {'wbthomason/packer.nvim', opt = true}
  use 'shaunsingh/nord.nvim'                                            -- A colorscheme
  use 'mhartington/formatter.nvim'                                      -- A formatter client
  use 'feline-nvim/feline.nvim'                                         -- A status line
  use 'kyazdani42/nvim-web-devicons'                                    -- Better icons
  use 'leafOfTree/vim-vue-plugin'
  use 'pangloss/vim-javascript'
  use {                                                                 -- A gutter for git
    'lewis6991/gitsigns.nvim',
    requires = {'nvim-lua/plenary.nvim'},
    config = function()
      require('gitsigns').setup()
    end
  }
  use {
  "folke/trouble.nvim",
  requires = "kyazdani42/nvim-web-devicons",
  config = function()
    require("trouble").setup {}
  end
}
  use 'cespare/vim-toml'                                                -- Support for TOML
  use 'nvim-treesitter/nvim-treesitter'                                 -- Language parser improving syntax highlight
  use 'neovim/nvim-lspconfig'                                           -- LSP client configuration
  use 'tpope/vim-fugitive'                                              -- Git plugin
  use {'ms-jpq/coq_nvim', branch = 'coq'}                               -- Completion plugin
  use {'ms-jpq/coq.artifacts', branch = 'artifacts'}                    -- Completion snippets
  use 'mcchrish/nnn.vim'                                                -- File explorer
  use 'tpope/vim-commentary'                                            -- Commentary plugin
  use 'pearofducks/ansible-vim'                                         -- Ansible plugin
  use 'jiangmiao/auto-pairs'                                            -- Autoclose bracket
  use 'dhruvasagar/vim-table-mode'                                      -- Table mode for markdown
  use 'ixru/nvim-markdown'                                              -- markdown support
  use 'mustache/vim-mustache-handlebars'                                -- mustache support
  use {                                                                 -- fuzzy finder
    'nvim-telescope/telescope.nvim',
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
  }
  use 'mfussenegger/nvim-lint'                                          -- A linter engine.
end)

vim.g.mapleader = ' '
vim.bo.tabstop = 4                                                      -- One tab equals 4 spaces
vim.bo.shiftwidth = 4                                                   -- 4 spaces when indenting
vim.bo.expandtab = true                                                 -- Convert tab to x spaces
vim.o.mouse = ''
vim.o.ignorecase = true                                                 -- Ignore case when searching
vim.o.smartcase = true                                                  -- Do not ignore case with uppercase character
vim.o.incsearch = true                                                  -- Show pattern matched when searching
vim.o.inccommand = 'split'                                              -- Show result from incremental search in split buffer
vim.o.hidden = true                                                     -- Enable background buffers
vim.o.wildmenu = true                                                   -- Enables "enhanced mode" of command-line completion
vim.o.laststatus = 3                                                    -- Display only one status line for n split windows
vim.o.termguicolors = true                                              -- True color support
vim.o.grepprg = 'rg --vimgrep --smart-case --follow'                    -- Set ripgrep as grep program
vim.o.completeopt = 'menuone,noinsert,noselect'                         -- Show the completion menu even if there's one match
vim.o.showmode = false                                                  -- Do not show current mode, as already shown by statusline
vim.o.list = true
vim.opt.listchars = { tab = '▸ ', trail = '·' }
vim.wo.cursorline = true                                                -- Highlight current line
vim.wo.number = true                                                    -- Display line number
vim.wo.relativenumber = true                                            -- Display relative line number
vim.cmd 'colorscheme nord'
vim.cmd 'let g:markdown_fenced_languages = ["shell=sh"]'
vim.cmd 'highlight WinSeparator guibg=None'

require('feline').setup()                                               -- Status line configuration

require('formatter').setup({                                            -- Formatter configuration
  logging = false,
  filetype = {
    python = {
      function()
        return {
          exe = 'black',
          args = {'-'},
          stdin = true
        }
      end
    }
  },
  filetype = {
    yaml = {
      function()
        return {
          exe = 'prettier',
          args = {
            '--stdin-filepath',
            formater.util.escape_path(formatter.util.get_current_buffer_file_path()),
            stdin = true,
            try_node_modules = true
          },
          stdin = true
        }
      end
    }
  },
  filetype = {
    vue = {
      function()
        return {
          exe = "prettier",
          args = {
            "--stdin-filepath",
            vim.fn.fnameescape(vim.api.nvim_buf_get_name(0)),
            '--single-quote'
          },
          stdin = true
        }
      end
    },
  }
})

vim.api.nvim_set_keymap(
  'n', '<leader>f', ':Format<CR>', {noremap = true}
)

vim.api.nvim_set_keymap("n", "<leader>q", "<cmd>TroubleToggle document_diagnostics<CR>",
  {silent = true, noremap = true}
)

local treesitter = require'nvim-treesitter.configs'                     -- Configure treesitter to use
treesitter.setup {                                                      -- highlight, indentation and folding features
  ensure_installed = 'all',
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = {'php'}
  },
  indentation = {enable = true},
  folding = {enable = true}
}
-- LSP Configuration
local lsp = require'lspconfig'
local coq = require"coq"


local on_attach = function(client, bufnr)
  local function buf_set_keymap(...)
      vim.api.nvim_buf_set_keymap(bufnr, ...)
  end
  local function buf_set_option(...)
      vim.api.nvim_buf_set_option(bufnr, ...)
  end

  local opts = { noremap=true, silent=true }

  buf_set_keymap(
    'n',
    'gD',
    '<Cmd>lua vim.lsp.buf.declaration()<CR>',
    opts
  )
  buf_set_keymap(
    'n',
    'gd',
    '<Cmd>lua vim.lsp.buf.definition()<CR>',
    opts
  )
  buf_set_keymap(
    'n',
    'gr',
    '<cmd>lua vim.lsp.buf.references()<CR>',
    opts
  )
  buf_set_keymap(
    'n',
    '<space>n',
    '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>',
    opts
  )
  buf_set_keymap(
    'n',
    '<space>p',
    '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>',
    opts
  )
  buf_set_keymap(
    'n',
    '<space>rn',
    '<cmd>lua vim.lsp.buf.rename()<CR>',
    opts
  )
end

local lsp_servers = {
  "ansiblels", "jedi_language_server", "rust_analyzer", "eslint", "vuels", "tsserver"
}                      -- LSP languages configuration
for _, lsp_server in ipairs(lsp_servers) do
  lsp[lsp_server].setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    }
  }))
end

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    -- This will disable virtual text, like doing:
    -- let g:diagnostic_enable_virtual_text = 0
    virtual_text = true,

    -- This is similar to:
    -- let g:diagnostic_show_sign = 1
    -- To configure sign display,
    --  see: ":help vim.lsp.diagnostic.set_signs()"
    signs = true,

    -- This is similar to:
    -- "let g:diagnostic_insert_delay = 1"
    update_in_insert = true,
  }
)

require('lint').linters_by_ft = {
  python = {'pylint',}
}

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  callback = function()
    require("lint").try_lint()
  end,
})

vim.api.nvim_set_keymap('n', '<F3>', ':NnnPicker<CR>', {noremap = true})
vim.api.nvim_set_keymap(
  'n', '<C-P>',
  '<cmd>lua require(\'telescope.builtin\').find_files()<cr>',
  {noremap = true}
)
vim.api.nvim_set_keymap(
  'n', '<C-F>',
  '<cmd>lua require(\'telescope.builtin\').live_grep()<cr>',
  {noremap = true}
)
vim.api.nvim_set_keymap(
  'n', '<C-B>',
  '<cmd>lua require(\'telescope.builtin\').buffers()<cr>',
  {noremap = true}
)
vim.api.nvim_set_keymap(
  'n', '<leader>r',
  '<cmd>lua require(\'telescope.builtin\').lsp_references()<cr>',
  {noremap = true}
)


vim.keymap.set('v', 'y', 'myy`y')                                       -- Maintain the cursor position when yanking a visual selection
vim.keymap.set('v', 'Y', 'myY`y')

vim.g['nnn#layout'] = {
  window = {
    width = 0.9,
    height = 0.6,
    highlight = 'Debug'
  }
}
